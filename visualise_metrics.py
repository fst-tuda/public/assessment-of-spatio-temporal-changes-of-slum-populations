# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 11:00:06 2021

@author: julius breuer

This script loads the multiple metrics.csv files created from "evaluate_dynamics_metrics.py"
and (i)     creates barplots of the three mobility indicators GDF, GLF, MoC for all files
and (ii)    creates barplots of the total changes of number of slums, area in km^2 and population in mio.

"""


#%% load values

import matplotlib.pyplot as plt
import pandas as pd

colors =  [[220/255]*3,[181/255]*3,[137/255]*3,[83/255]*3]



#%% USER INPUT
saved_metrics_folder = [r"folder\to\file1",
                        r"folder\to\file2"]

names = ['file1','file2']


#%% load metrics from different sources into a single GeoDataFrame

metrics = [pd.read_csv(x+'metrics.csv',index_col=0,names=[names[idx]],header=0) for idx,x in enumerate(saved_metrics_folder)]
metrics = pd.concat(metrics,axis=1).transpose()


#%% PLOT #slums, area and population as totals for all slums in the four cities.

def autolabel(rects1,rects2,increase,ldx):
    """Attach a text label above each bar in *rects*, displaying the value given in perce."""
    
    for jdx,(rect1,rect2) in enumerate(zip(rects1,rects2)):
        
        height = rect1.get_height() + rect2.get_height()
        axs[ldx].annotate("%0.f%%"%increase[jdx] ,
                    xy=(rect1.get_x() + rect1.get_width() / 2, height),
                    # xytext=(0, 3),  # 3 points vertical offset
                    # textcoords="offset points",
                    fontsize=10,
                    ha='center', va='bottom')

colors =  [[220/255]*3,[181/255]*3,[137/255]*3,[83/255]*3]


fig, axs = plt.subplots(1,3,figsize=(8,3))

metric_names = [['N1','N2','N_inc'],['A1','A2','A_inc'],['P1','P2','P_inc']]
bar_names = ["#SLUMS","AREA in $\mathrm{km^2}$","POPULATION\nin million"]

for idx in range(len(metric_names)):
    current_metric = metric_names[idx]
    rects1 = axs[idx].bar(names,metrics.loc[:,current_metric[0]],color=colors,edgecolor="black")
    rects2 = axs[idx].bar(names,metrics.loc[:,current_metric[1]],bottom =metrics.loc[:,current_metric[0]],color=colors,edgecolor="black")
    axs[idx].set_ylabel(bar_names[idx])

    autolabel(rects1,rects2,list(metrics.loc[:,current_metric[2]]),idx)

fig.tight_layout()

# %% PLOT POPULATION DENSITIES FOR THE FIVE AREA CLASSES

fig, axs = plt.subplots(1,len(names),figsize=(10,4), sharey=True)

for jdx in range(len(metrics)):
    ax = axs[jdx]
    metric = metrics.iloc[jdx,:]

    years = metric[["YEAR1","YEAR2"]]
    ax.bar(0,metric["shrunk density"],hatch="...",color="lightgreen",
           edgecolor="black",label="SHRUNK")
    ax.bar(1,metric["disappeared density"],color="yellow",
           edgecolor="black",label="DISAPPEARED")

    ax.bar(2,metric["persistent y1 density"], color="lightgreen",
           edgecolor="black", label="PERSISTENT")
    ax.bar(4,metric["persistent y2 density"],color="lightgreen",
           edgecolor="black", label="_nolegend_")

    ax.bar(5,metric["emerged density"],color="lightgreen",hatch="//",
           edgecolor="black", label="GROWN")
    ax.bar(6,metric["grown density"],color="blue",
           edgecolor="black", label="EMERGED")

    ax.set_title(names[jdx])
    ax.set_xticks([1,5],years.astype(int))


ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
axs[0].set_ylabel("POPULATION DENSITY in inh./$km^2$")
fig.text(0.5, 0.01, 'YEAR', ha='center')
fig.tight_layout()


#%% PLOT dynamics
# #distribution of population according to their dynamics in between the two points in time
fig,axs = plt.subplots(1,len(metrics),figsize=(2*len(metrics),3.3))

for idx in range(len(metrics)):
       ax = axs[idx]
       metric = metrics.iloc[idx,:]
       x1,x2 = 0,1
       years = metric[["YEAR1","YEAR2"]]
       ax.bar(x1,metric["Baseline"],color="lightgreen",edgecolor="black")
       ax.bar(x1,metric["Shrunk"],bottom = metric["Baseline"],color="lightgreen",hatch="...",edgecolor="black")
       ax.bar(x1,metric["Disappeared"],bottom = metric["Baseline"] + metric["Shrunk"],color="yellow",edgecolor="black")

       ax.bar(x2,metric["Baseline"],color="lightgreen",edgecolor="black")
       ax.bar(x2,metric["Densify"],bottom=metric["Baseline"],color="lightgreen",edgecolor="black")
       ax.bar(x2,metric["Growth"],bottom =metric["Baseline"]+metric["Densify"],color="lightgreen",hatch="//",edgecolor="black")
       ax.bar(x2,metric["Emerge"],bottom =metric["Baseline"]+metric["Densify"]+metric["Growth"],color="blue",edgecolor="black")

       ax.plot([x1,x2],[metric["Baseline"],metric["Baseline"]],"--",color="black",zorder=-1,linewidth=0.2)

       ax.set_ylabel("POPULATION in mio.")

       ax.set_xticks([x1,x2])
       ax.set_xticklabels(years.astype(int))
       ax.ticklabel_format(style='sci', axis='y')
       ax.set_title(metric.name)

fig.text(0.5, 0.01, 'YEAR', ha='center')
fig.tight_layout()


#%% plot metrics

fig, axs = plt.subplots(1,3,figsize=(6,3),sharey=True)

metric_names = ['Growth Driving Factor','Growth Location Factor','Momentum of Change']

for idx in range(len(axs)):
    axs[idx].bar(names,metrics.loc[:,metric_names[idx]],color=colors,edgecolor="black")
    axs[idx].set_ylabel(metric_names[idx])
    axs[idx].set_yscale("log")
    axs[idx].axhline(1,linestyle="--",color="black")

axs[idx].set_ylim((1e-2,1e2))
fig.tight_layout()