# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 11:00:06 2021

@author: julius breuer

This script takes the slum files from "create_shapefile_multi.py"
and divides the slum units according to their population's spatio-temporal changes

saves slum data files as shapefiles, containing the population, area, density and all geodata included in a shapefile
persistent_slums_YEAR   : geodataframe of persistent slums in the corresponding YEAR
emerged_slums           : geodataframe of emerged slums (only of the second year, as slums could only *emerge* there)
disappeard_slums (only 1. year), grown_slums (only 2. year), shrunk_slums (only 1. year)
"""


#%% load packages

from Basic_Functions import clipRasterByShape, get_population_by_rasterfolder

import geopandas as gpd



#%% USER INPUT
gridpops = [r"path\to\gridded_population_dataset_of_first_year",
            r"path\to\gridded_population_dataset_of_second_year"]

years = [2000,2010]

slumpop_outfolder = r"working/directory"


# %% input shapefiles for both years

n_timestamps = len(years)


dfs = []
for idx in range(n_timestamps):
    dfs.append(gpd.read_file(slumpop_outfolder+"all_slums_%i.shp"%years[idx]))



#%% divide slum geometries into the five types of dynamics (areal changes)

emerged_slums = dfs[1].loc[dfs[1]["sns_path"]==1]
disappeared_slums = dfs[0].loc[dfs[0]["sns_path"]==2]

for idx in range(2):
    dfs[idx] = dfs[idx].filter(["sns_path","geometry","slum_unit"])

persistent_slums = gpd.overlay(dfs[0],dfs[1],how="intersection")
grown_slums = gpd.overlay(dfs[1].loc[dfs[1]["sns_path"]==3],persistent_slums,how="difference")
shrunk_slums = gpd.overlay(dfs[0].loc[dfs[0]["sns_path"]==3],persistent_slums,how="difference")



#%% create rasterfiles for each type of dynamics

outfolder = slumpop_outfolder+"persistence\\"
os.mkdir(outfolder)
persistent_slums = clipRasterByShape(gridpops, [persistent_slums,persistent_slums], outfolder,
                ["Pop%i"%year for year in years],buffer=0,
                buffer_flag=False,intersect_raster_flag=False)  

outfolder = slumpop_outfolder+"grown\\"
os.mkdir(outfolder)
grown_slums = clipRasterByShape([gridpops[1]], [grown_slums], outfolder,
                ["Pop%i"%years[1]],buffer=0,
                buffer_flag=False,intersect_raster_flag=False)[0]

outfolder = slumpop_outfolder+"shrunk\\"
os.mkdir(outfolder)
shrunk_slums = clipRasterByShape([gridpops[0]], [shrunk_slums], outfolder,
                ["Pop%i"%years[0]],buffer=0,
                buffer_flag=False,intersect_raster_flag=False)[0]


#%% recalculate population, area and population density
outfolder = slumpop_outfolder+"persistence\\"
for idx in range(2):
    persistent_slums[idx]["population"] = get_population_by_rasterfolder(outfolder+"Pop%i\\"%years[idx], 16)[0]
    persistent_slums[idx]["area"]=persistent_slums[idx].to_crs("epsg:6933")["geometry"].area/1e6
    persistent_slums[idx]["density"]=persistent_slums[idx]["population"] / persistent_slums[idx]["area"]

outfolder = slumpop_outfolder+"grown\\"
grown_slums["population"] = get_population_by_rasterfolder(outfolder+"Pop%i\\"%years[1], 16)[0]
grown_slums["area"]=grown_slums.to_crs("epsg:6933")["geometry"].area/1e6
grown_slums["density"]=grown_slums["population"] / grown_slums["area"]

outfolder = slumpop_outfolder+"shrunk\\"
shrunk_slums["population"] = get_population_by_rasterfolder(outfolder+"Pop%i\\"%years[0], 16)[0]
shrunk_slums["area"]=shrunk_slums.to_crs("epsg:6933")["geometry"].area/1e6
shrunk_slums["density"]=shrunk_slums["population"] / shrunk_slums["area"]



# %% save files


shrunk_slums.to_file(slumpop_outfolder+"shrunk_slums.shp")
grown_slums.to_file(slumpop_outfolder+"grown_slums.shp")

persistent_slums[0].to_file(slumpop_outfolder+"persistent_slums_%i.shp"%years[0])
persistent_slums[1].to_file(slumpop_outfolder+"persistent_slums_%i.shp"%years[1])

emerged_slums.to_file(slumpop_outfolder+"emerged_slums.shp")
disappeared_slums.to_file(slumpop_outfolder+"disappeared_slums.shp")

