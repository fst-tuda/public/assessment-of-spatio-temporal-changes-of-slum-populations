# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 17:11:24 2021

@author: julius breuer

An explanation of all scripts needed for evaluating slum population dynamic metrics is given here:
https://git.rwth-aachen.de/fst-tuda/projects/urbanization/mt-breuer/-/wikis/Slum-Interaction-according-to-%22Spatio-Temporal-Changes-of-Slum-Populations%22

This script takes shapefiles, gridded population datasets
and a working directory and outputs the same shapefiles with added attributes:
- the population (according to the gridded population dataset)
- the area (is implicitly given in every shapefile, here added explicit)
- the population density
- the sns_path (see gitlab wiki for mt breuer for more information)

this script could be used to evaluate slum dynamics of multiple years, more than two is possible.
However, the remaining scripts in this folder are not optimized for more than 2 years.

"""
#%% load packages

from Basic_Functions import mergeBygeometry, clipRasterByShape, \
        get_population_by_rasterfolder, buffer_gdf, intersection_ids

import geopandas as gpd
import numpy as np


#%% USER INPUT

# according year of the shapefile and gridded population dataset
years = [2000,2010]

# shapefiles
shps_in =  [r"path\to\shapefiles_of_first_year",
            r"path\to\shapefiles_of_second_year"]

# gridded population dataset
gridpops = [r"path\to\gridded_population_dataset_of_first_year",
            r"path\to\gridded_population_dataset_of_second_year"]

# working directory (will be created)
slumpop_outfolder = r"working\directory\"



#%% load shapefile
os.mkdir(slumpop_outfolder)

n_timestamps = len(gridpops)

dfs_raw = [gpd.read_file(x) for x in shps_in]
crs = dfs_raw[0].crs


#%% merge slum geometries, when touching
dfs_merged = [mergeBygeometry(x) for x in dfs_raw]

# add more information (potentially unnecessary) and create GeoDataFrame from dfs_merged
for idx in range(n_timestamps):
    dfs_merged[idx] = gpd.GeoDataFrame(dfs_merged[idx])
    dfs_merged[idx].columns = ["geometry"]
    dfs_merged[idx].crs = crs # change epsg to fit the research area
    dfs_merged[idx]["FID"]=np.int64(dfs_merged[idx].index)


#%% resample and clip raster by shapefiles, merging touching shapes
print("Resample and clip raster by shapefiles...")

dfs_merged = clipRasterByShape(gridpops, dfs_merged, slumpop_outfolder,
                    ["Pop%i"%year for year in years],buffer=0,
                    buffer_flag=True,intersect_raster_flag=False)  
    
#%% calculate population in shapefiles
print("Calculate population in shapefiles")

popcounts = [get_population_by_rasterfolder(slumpop_outfolder+"Pop%i\\"%x,16)[0] for x in years]

#%% add area and population to existing geodataframes
print("Calculate area and add population and area to GDF")

dfs = dfs_merged.copy()
for idx in range(n_timestamps):
    dfs[idx]["area"]=dfs[idx].to_crs("epsg:6933")["geometry"].area/1e6
    dfs[idx]["population"]=popcounts[idx]


#%% exclude not habitated slums

print("Exclude slums with population < 1")

excluded = [np.sum(x['population']<1)  for x in dfs]

print(f"Number of slums excluded: {excluded}")

# delete all slums that are smaller than 1
for idx in range(n_timestamps):
    dfs[idx] = dfs[idx].loc[dfs[idx]["population"]>=1]

#%% Create union of all layers. This is needed to analyse slum dynamics
print("Create new gdf as union of all layers")
dfs_union = dfs[0].dissolve()
for idx in range(1,n_timestamps):
    dfs_union = gpd.overlay(dfs_union.dissolve(),dfs[idx].dissolve(),how="union")
dfs_union = dfs_union.dissolve()

dfs_union2 = buffer_gdf(dfs_union,0.1)
dfs_union2["FID"]=np.int64(dfs_union2.index)


#%% calculate intersection of each layer with the union layer
# this is needed to create slums as defined in https://dx.doi.org/10.2139/ssrn.4106192
# in contrast to the paper, the newly defined slums are called slum units
print("Calculate intersection of all layers with union_layer")
for idx in range(n_timestamps):
    dfs[idx]["slum_unit"] = intersection_ids(dfs[idx],dfs_union2)


#%%
# assigns slum unit numbers to the different slums and aggregate all slums by slum_unit
dfs_su=[]
for idx in range(n_timestamps):
    dfs[idx]["#slums"]=1
    dfs_su.append(dfs[idx].dissolve(by="slum_unit",aggfunc="sum"))


#%% Add year and population density values
print("Add columns...")

for idx in range(n_timestamps):
    # add year
    dfs_su[idx]["year"] = int(years[idx])
    # add population density as inh./km^2
    dfs_su[idx]["density"] = dfs_su[idx]["population"] / dfs_su[idx]["area"]
   
 
#%% Create SNS
print("Add sns_path values to each slum_unit")
sns = []
for idx in range(len(dfs_union2)):
    a = 0
    for jdx in range(n_timestamps):
        a = a + (idx in list(dfs_su[jdx].index))*2**(n_timestamps-1-jdx)
    sns.append(a)
    

    for jdx in range(n_timestamps):
        if idx in list(dfs_su[jdx].index):
            dfs_su[jdx].loc[dfs_su[jdx].index==idx,"sns_path"]=a
            
    dfs_union2.loc[idx,"sns_path"]=a


#%% Saving files in working directory

for idx in range(n_timestamps):
    dfs_su[idx].to_file(slumpop_outfolder+"all_slums_%i.shp"%years[idx])
