# assessment of spatio-temporal changes of slum populations


## Description
This repository is used for processing the data and visualising the metrics derived in the publication "Methods to assess Spatio-Temporal Changes of Slum Populations" (https://dx.doi.org/10.2139/ssrn.4106192).

## Installation
In order to use the scripts, besides standard librabries like numpy, matplotlib and pandas, the installation of geopandas and rasterio is necessary.
The authors used rasterio version 1.2.10 and geopandas 0.10.2, upward compatibility is likely.

## Usage
The following flow chart explains how to use the code for analysing slum population mobility.
![Flow chart explaining how to use the code.](explanation_of_scripts.png){width=60%,height:60%}

## Authors and acknowledgment (CRediT author statement)
Julius Breuer - Conceptualization, Methodology, Software, Validation, Formal analysis, Investigation, Resources, Data Curation

John Friesen - Conceptualization, Methodology, Formal analysis, Resources, Supervision, Project administration


## License
The code is licensed under The MIT License (MIT).

## Project status
The project is further developed as of 04.2023.
