# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 11:00:06 2021

@author: julius breuer

This script takes the slum files from "slum_nonslum_dynamics.py"
and (i)     plots population bar graphs according to spatio-temporal dynamics
and (ii)    plots histogram of the persistent population and emerging slum population
and (iii)   saves all metrics (incl. mobility indicators and total changes) as a Panda Series


"""
#%% load packages

import geopandas as gpd
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt


#%% USER INPUT

# folder containing the slum shapefiles
slumpop_outfolder = r"working/directory"
years = [2000,2010] #years as integers



#%% load slum shapefiles
year_gap = years[1] - years[0]

shrunk_slums = gpd.read_file(slumpop_outfolder+"shrunk_slums.shp")
grown_slums = gpd.read_file(slumpop_outfolder+"grown_slums.shp")

persistent_slums = [gpd.read_file(slumpop_outfolder+"persistent_slums_%i.shp"%x) for x in years]

emerged_slums = gpd.read_file(slumpop_outfolder+"emerged_slums.shp")
disappeared_slums = gpd.read_file(slumpop_outfolder+"disappeared_slums.shp")

all_slums = []
for idx in range(len(years)):
    all_slums.append(gpd.read_file(slumpop_outfolder+"all_slums_%i.shp"%years[idx]))



#%% calculate slum populations

shrunk_pop = shrunk_slums.population.sum()/1e6
grown_pop = grown_slums.population.sum()/1e6
persistent_pop = [x.population.sum()/1e6 for x in persistent_slums]
emerged_pop = emerged_slums.population.sum()/1e6
disappeared_pop = disappeared_slums.population.sum()/1e6


##% calculate slum population density means

SH_D = shrunk_slums.density.mean()
G_D = grown_slums.density.mean()
PE_D = [x.density.mean() for x in persistent_slums]
E_D = emerged_slums.density.mean()
DI_D = disappeared_slums.density.mean()



#%% CALCULATE METRICS

## calculate total changes

# population in mio. people
P1 = all_slums[0].population.sum()/1e6 #same as: persistent_pop[0] + disappeared_pop + shrunk_pop
P2 = all_slums[1].population.sum()/1e6 #same as: persistent_pop[1] + emerged_pop + grown_pop
N1 = len(all_slums[0])
N2 = len(all_slums[1])

# area in km^2
A1 = all_slums[0]["area"].sum()
A2 = all_slums[1]["area"].sum()

# population increase in 10 years
P_inc = ((P2/P1)**(10/year_gap)-1)*100
# increase in number of slums in 10 years
N_inc = ((N2/N1)**(10/year_gap)-1)*100
# areal increase in 10 years
A_inc = ((A2/A1)**(10/year_gap)-1)*100

## calculate mobility indicators as described in the paper
B = persistent_pop[0] # baseline
DS = persistent_pop[1] - persistent_pop[0] # densified
SH = shrunk_pop
DI = disappeared_pop
D = SH + DI # declined population
G = grown_pop
E = emerged_pop

# Growth Driving Factor
GDF = DS/P2 / ((E+G)/P2 - D/P1)

# Growth Location Factor
GLF = E/G

# Momentum of Change
MoC = (G+E)/P2/(D/P1)




#%% save metrics (e.g. mobility indicators)
keys = ["YEAR1", "YEAR2", 'N1','N2','N_inc','A1','A2','A_inc','P1','P2','P_inc',
        "Baseline","Densify","Shrunk","Disappeared","Decline","Growth",'Emerge',
        "shrunk density", "disappeared density", "persistent y1 density",
        "persistent y2 density", "grown density", "emerged density",
        'Growth Driving Factor','Growth Location Factor','Momentum of Change']
values = np.asarray([years[0], years[1], N1,N2,N_inc,A1,A2,A_inc,P1,P2,P_inc,
                     B,DS,SH,DI,D,G,E,
                     SH_D, DI_D, *PE_D, G_D, E_D,
                     GDF,GLF,MoC])

series = pd.Series(values, index= keys)

series.to_csv(slumpop_outfolder+'metrics.csv')


#%% PLOT dynamics
# #distribution of population according to their dynamics in between the two points in time
fig,ax = plt.subplots(1,1,figsize=(2,3))

x1,x2 = 0,1
ax.bar(x1,B,color="lightgreen",edgecolor="black")
ax.bar(x1,SH,bottom =B,color="lightgreen",hatch="...",edgecolor="black")
ax.bar(x1,DI,bottom = B+SH,color="yellow",edgecolor="black")

ax.bar(x2,B,color="lightgreen",edgecolor="black")
ax.bar(x2,DS,bottom=B,color="lightgreen",edgecolor="black")
ax.bar(x2,G,bottom =B+DS,color="lightgreen",hatch="//",edgecolor="black")
ax.bar(x2,E,bottom =B+DS+G,color="blue",edgecolor="black")

ax.plot([x1,x2],[B,B],"--",color="black",zorder=-1,linewidth=0.2)

ax.set_ylabel("POPULATION in mio.")
ax.set_xlabel("YEAR")

ax.set_xticks([x1,x2])
ax.set_xticklabels(years)
ax.ticklabel_format(style='sci', axis='y')

fig.tight_layout()
plt.show()


#%% PLOT histogram of change
# frequency of slum population for all slums in the two time points as well as the slum population emerging in between


fig,ax = plt.subplots(figsize=(4,3),sharex=True)
yticks = np.array([-200,0,200])

colors = ["lightgrey","grey"]

bins=15

for idx in range(2):
    counts, bins         =  np.histogram(np.log10(all_slums[idx]["population"]), bins=bins)
    ax.hist(10**bins[:-1],10**bins,weights=counts*(-1)**(idx),color=colors[idx], rwidth=0.95)
    if idx == 1:    
        counts,_ =  np.histogram(np.log10(all_slums[idx].loc[all_slums[idx].sns_path==1,"population"]), bins=bins)
        ax.hist(10**bins[:-1],10**bins,weights=counts*-1,color="blue", rwidth=0.95)

ax.set_xscale("log")
ax.set_xticks([10,1000,1e5])
ax.set_yticks(yticks)
ax.set_yticklabels(abs(yticks))
ax.axhline(0,color="black")
    
ax.set_xlabel("SLUM POPULATION", ha='center')
ax.set_ylabel("FREQUENCY", va='center', rotation='vertical')

