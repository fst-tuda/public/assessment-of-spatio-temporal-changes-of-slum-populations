# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 08:09:31 2021

@author: julius breuer
"""

import re
import os

import numpy as np

import rasterio
from rasterio.enums import Resampling
from rasterio import Affine
import rasterio.mask

import geopandas as gpd


def natural_key(string):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string)]

# use with geopandas
def mergeBygeometry(gdf):
    """
    Function that merges a given GeoDataFrame (gdf) such that all touching geometries are merged together.
    Finally, all not-touching shapes are single gdf entries.

    Parameters
    ----------
    gdf : GeoDataFrame
        DESCRIPTION.

    Returns
    -------
    TYPE
        GeoSeries containing not-touching geometries.

    """
    return gpd.geoseries.GeoSeries([geom for geom in gdf.unary_union.geoms])


def buffer_gdf(df,buffer=10):
    """
    Takes account of the right crs and returns a proper GDF.

    Parameters
    ----------
    df : GeoDataFrame

    buffer : int, optional
        Buffer value in meter. The default is 10.

    Returns
    -------
    shapes : GeoDataFrame
        DESCRIPTION.

    """
    crs = df.crs
    shapes = df.to_crs("epsg:6933")
    shapes = shapes.buffer(buffer,5)
    shapes = shapes[shapes != None]
    shapes = mergeBygeometry(shapes)
    shapes = shapes.buffer(-buffer,5)
    shapes = gpd.GeoDataFrame(geometry = shapes)
    shapes.columns = ["geometry"]
    shapes.crs = "epsg:6933"
    shapes = shapes.to_crs(crs)
    return shapes

def intersection_ids(old,new):
    """
    Calculates the intersection of two GDF.

    Parameters
    ----------
    old : GeoSeries
        Containing geometry polygons.
    new : GeoSeries
        Containing geometry polygons.

    Returns
    -------
    GeoDataFrame
         Returns a list containing indizes of intersecting polygons.


    """
    old = old["geometry"]
    new = new["geometry"]
    intersection = []
    for idx in list(old.index):
        for jdx in list(new.index):
            if old[idx].intersects(new[jdx]):
                intersection.append(jdx)
                break

    return intersection 

def clipRasterByShape(rasterfiles,shapes, out_folder, rasternames, 
                             buffer_flag = False, buffer = 0,resize_scale=16, single_shape = False,
                             intersect_raster_flag = True):
    """
    This function takes a rasterfile and a gdf (possibly a shapefile) and cuts all raster values that lay inside the shapes, 
    proceeding shape by shape and storing the resulting clipped raster in a GeoTiff File.
    Finally the output the same shapefiles as the input shapefiles with population counts added for each slum.
    The saving location is out_folder.
    
    Parameters
    ----------
    rasterfiles : list of strings
        Containing the paths to the rasterfiles.
    shapes : GeoDataFrame
        Containing the shapes to clip the rasterfile.
    out_folder : string
        Path to store the clipped rasterfiles.
    rasternames : list of strings, optional
        Name of the Rasterfiles folder that is created inside out_folder. The default is "Pop".
    code_flag : Boolean, optional
        If all shapes have a unique code (normally not the case), this code can be used to store the Rasterfiles in a file named by the corresponding code. The default is False.
    codename : string, optional
        Name of the code, see code_flag. The default is "Code".
    buffer_flag : Boolean, optional
        Buffers the shapes if True. Directly reassignes the shape into a GDF. The default is False.
    buffer : int, optional
        Defining the buffer range in meters. If 0 then no buffersize is used. The default is 0.

    Raises
    ------
    AssertionError
        If code_flag and buffer_flag are on. Buffering will merge shapes thus remove the code ordering and so it would be useless.

    Returns
    shapes : GeoDataFrames that were used as input, yet population count and population density is added to the slum files.
    None.

    """

    if not isinstance(shapes,list):
        shapes = [shapes]
    
    # buffering the shapes. 
   
    for idx in range(len(shapes)):
        if buffer_flag: shapes[idx] = buffer_gdf(shapes[idx],buffer)
        shapes[idx] = shapes[idx].dropna(subset=["geometry"]) # delete rows where geometry is none
    
    
    for idx in range(len(rasterfiles)):
        raster = rasterio.open(rasterfiles[idx])
        shape = shapes[idx]
        shape["FID"]= np.arange(len(shape))
        # create new folder
        os.mkdir(out_folder + rasternames[idx])
        shape = shape.to_crs(raster.crs.data["init"]) # transform crs of shapes to that of the rasterfile
        
        # create list containing all shapes geometries
        lst_shapes = list(shape["geometry"])
        
        # Now the following procedure for clipping is used in order to save disk space:
        #   - first, the raster is clipped by all shapes, using all rastercells that touch the shapes.
        
        clip_raster(raster, lst_shapes, out_folder + "intermediate.tif", all_touched = True)
        raster = rasterio.open(out_folder + "intermediate.tif")
        
        #   - second the resulting raster is scaled by 16 (so a 1x1 cell is scaled to 16x16 cells, using the 1x1 value for all 16x16 cell values (no division))
        # scale = 16
        resample_raster(raster, out_folder + "intermediate2.tif", scale=resize_scale)
        raster = rasterio.open(out_folder + "intermediate2.tif")
        
        if intersect_raster_flag:
            polygonized,_,_,_,_ = polygonize_raster(out_folder + "intermediate2.tif")
            polygonized = polygonized.loc[polygonized["raster_val"]>0] # cut off all raster cells that contain no persons.
            polygonized.to_crs(shape.crs)
            shape = gpd.overlay(shape,polygonized.dissolve(),how="intersection")
        
        
        if single_shape == False:
            
            lst_shapes = list(shape["geometry"])
            
    
            for jdx,shape_1 in enumerate(lst_shapes):
                # shape numbering according to the codes.
                raster_out = out_folder + rasternames[idx] + "\\Slum_Pop_%s.tif"%jdx
                # see - third
                clip_raster(raster, [shape_1], raster_out)        
        else:
            raster_out = out_folder + rasternames[idx] + "\\Pop.tif"
            shape = shape.dissolve()
            clip_raster(raster,list(shape["geometry"]),raster_out)
        
        # releasing the rasterfile
        del raster 
        
        shapes[idx] = shape
    
    try:
        shape.to_file(out_folder + rasternames[idx] + "\\used_shapefile.shp")
    except:
        print("Couldn't properly save Shapefile")
    
    return shapes

def clip_raster(raster, shapes, filename, all_touched=False):
    """
    Clips (cuts) the raster by a shapefile.
    note: Pad is not used as it only changes the outermost values of the raster geometry

    Parameters
    ----------
    raster : rasterio dataset
        possibly obtained by rasterio.open(*).
    shapes : list of multiple (or 1) GeoSeries which contain geometries only.
            If you struggle creating the right input "shapes" dataset, one example is using gdf= gpd.read_file(filename) and then list(gdf["geometry"])
        
    filename : string
        The resulting clipped raster is saved to filename.
    all_touched : Boolean, optional
        If True: All rastercells that touch the shape are taken.
        If False: Only rastercells whose central points lay inside the shape are taken. The default is False.

    Returns
    -------
    None.

    """
    out_image, out_transform = rasterio.mask.mask(raster, shapes, crop=True, all_touched=all_touched)
    out_meta = raster.meta
    out_meta.update({"driver": "GTiff",
                     "height": out_image.shape[1],
                     "width": out_image.shape[2],
                     "transform": out_transform})
    
    with rasterio.open(filename,'w', **out_meta) as dataset:
        dataset.write(out_image)

def polygonize_raster(rasterfilename):
    mask = None
    with rasterio.Env():
        with rasterio.open(rasterfilename) as src:
            image = src.read(1) # first band
            transform = src.transform
            crs = src.crs
            nodataval = src.nodata
            results = (
            {'properties': {'raster_val': v}, 'geometry': s}
            for i, (s, v) 
            in enumerate(
                rasterio.features.shapes(image, mask=mask, transform=src.transform)))
            
    geoms = list(results)
    
    polygonized = gpd.GeoDataFrame.from_features(geoms)
    polygonized.crs = crs
    return polygonized,crs,transform, nodataval, image.shape



#%% Working with raster files only



def create_1darray(dataset):
    """
    Returns a 1d array of all values inside dataset that are not "no data" values.

    Parameters
    ----------
    dataset : rasterio dataset
        possibly obtained by rasterio.open(*).

    Returns
    -------
    Numpy-1xN array
        Array containing all raster cell values.

    """
    # 
    
    return dataset.read().squeeze()[dataset.dataset_mask().squeeze()==255]


def get_population_by_rasterfolder(folderpath,scale=1):
    """
    Calculates population for all *.tif files inside a given folder. Therefore dividing through scale^2 possibly accounting for resampled rasters.

    Parameters
    ----------
    folderpath : string
        Path of folder containing *.tif files.
    scale : int
        Scale possibly used for resampling the raster. The default is 1.

    Returns
    -------
    popcount : float
        Float value containing the population count of the raster.
    files : string
        Containing the *.tif files used, in the correct order.

    """
    
    # search for all *.tif files inside the path
    files = os.listdir(folderpath)
    files = [x for x in files if x[-3:] == "tif"]
    # sort the files by their natural numbering 1, 2, ..., 10, 11, .. instead of 1, 10, 11, .., 2, ...
    files = sorted(files, key = natural_key)
    
    popcount = []
    for file in files:
        popcount.append(np.sum(create_1darray(rasterio.open(folderpath + file)))/scale**2)
    return popcount, files


def resample_raster(raster, filename, scale=2):
    """
    Overly complex looking function that simply scales a given raster and saves it into the location defined by "filename".
    It simply assigns the value of the raster to all scaled rastercells.
    (e.g. 1x1 raster scaled to 16x16 raster containing all the same values equal to that of the 1x1 raster)

    Parameters
    ----------
    raster : rasterio dataset
    filename : string
    scale : integer, optional
        Scaling factor. 2 results in a 2x2 scaling. The default is 2.

    Returns
    -------
    None as it stores the resulting file directly.

    """
    t = raster.transform

    # rescale the metadata
    transform = Affine(t.a / scale, t.b, t.c, t.d, t.e / scale, t.f)
    height = raster.height * scale
    width = raster.width * scale
    #print(height,width)
    profile = raster.profile
    profile.update(transform=transform, driver='GTiff', height=height, width=width)

    data = raster.read( # Note changed order of indexes, arrays are band, row, col order not row, col, band
            out_shape=(raster.count, height, width),
            resampling=Resampling.nearest,
        )

    with rasterio.open(filename,'w', **profile) as dataset:
        dataset.write(data)
